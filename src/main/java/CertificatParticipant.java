
import org.bouncycastle.asn1.*;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.math.BigInteger;
import java.net.URL;
import java.security.*;
import java.security.cert.*;
import java.util.Date;
import java.util.List;


public class CertificatParticipant {
    private X509Certificate certificate= null;
    private String uriCA = "";

    /**
     * Constructeur initialisant le certificat du participant en fonction de l'uri de la TSL contenant le name du participant
     * @param uriTSL : uri de la TSL
     * @throws IOException
     * @throws CertificateException
     */
    public CertificatParticipant(String uriTSL) throws IOException, CertificateException {
        InputStream is;
        is = new URL(uriTSL).openStream();
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        this.certificate = (X509Certificate) cf.generateCertificate(is);

        readCertif rd = new readCertif(this.certificate);
        List<String> listuripartca = rd.getIssueCA();
        if(!listuripartca.isEmpty()){
            this.uriCA = listuripartca.get(0);
        }


    }

    public PublicKey getPublicKey(){
        return this.certificate.getPublicKey();
    }

    /**
     * Méthode de vérification générale du certificat participant (Date du certif / révocation / signature du certificat )
     * @param certifAC : certificat de l'AC
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     * @throws IOException
     * @throws CRLException
     */
    public void verify(CertificatAC certifAC) throws CertificateException, NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, CRLException {

        try {
            this.certificate.checkValidity(new Date());
            System.out.println("Certificat Participant pas encore expiré");
        } catch (CertificateExpiredException e){
            System.out.println("Certificat Expiré : " + e.getMessage());
        }

        readCertif rd = new readCertif(this.certificate);
        BigInteger serialnb = this.certificate.getSerialNumber();
        List<String> crluri = rd.getCRL();

        for(String crl : crluri){
            InputStream is = new URL(crl).openStream();
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509CRL crlcert = (X509CRL)cf.generateCRL(is);
            if(crlcert.getRevokedCertificate(serialnb)!=null){
                System.out.println("Erreur : Certif Participant est révoqué");
                break;
            }
        }
        System.out.println("Certif Participant n'est pas révoqué");
        boolean verifsignpart = verifySignature(certifAC);
        if(verifsignpart){
            System.out.println("Signature Certif Participant OK");
        }
        else {
            System.out.println("Signature Certif Participant PAS OK");
        }
    }

    /**
     * Méthode permettant de vérifier la signature du certificat du participant
     * @param certifAC : certificat de l'AC
     * @return true si la signature est OK false sinon
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws CertificateEncodingException
     * @throws SignatureException
     */
    public boolean verifySignature(CertificatAC certifAC) throws NoSuchAlgorithmException, InvalidKeyException, CertificateEncodingException, SignatureException {
        PublicKey pk = certifAC.getPublicKey();
        Signature sign =Signature.getInstance(this.certificate.getSigAlgName());
        sign.initVerify(pk);
        sign.update(this.certificate.getTBSCertificate());
        boolean b = sign.verify(this.certificate.getSignature());
        return b;
    }

    public String getUriCA() {
        return uriCA;
    }

    @Override
    public String toString() {
        return "CertificatParticipant{" +
                "certificate=" + certificate +
                '}';
    }
}
