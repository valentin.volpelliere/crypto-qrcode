import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class TwoDocReader{

    /**
     * Méthode permettant de décoder une image 2Ddoc
     * @param DdocFile : image 2Ddoc
     * @return une string coreespondant aux données du 2Ddoc
     * @throws IOException
     */
    static String decodeTwoDocCode(File DdocFile) throws IOException {
            BufferedImage bufferedImage = ImageIO.read(DdocFile);
            LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

            try {
                Result result = new MultiFormatReader().decode(bitmap);
                return result.getText();
            } catch (NotFoundException e) {
                System.out.println("There is no 2Ddoc code in the image");
            return null;
            }
     }


}
