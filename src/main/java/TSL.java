import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.security.*;
import java.security.cert.*;
import java.util.Date;
import java.util.List;


public class TSL {
    private X509Certificate certificate= null;
    private String uri;
    private String rootca="";

    /**
     * Constructeur permettant de recupérer le certif de la TSL ,la rootca si elle existe et de set l'uri permettant de récupérer les certifs participants.
     * @param ac ID de l'AC
     * @throws IOException
     * @throws CertificateException
     */
    public TSL(String ac) throws IOException, CertificateException {

        CertificateFactory fact = CertificateFactory.getInstance("X.509");
        FileInputStream is = new FileInputStream("src/main/resources/certifAC/certificatTSL.pem");
        this.certificate= (X509Certificate) fact.generateCertificate(is);
        setUri(ac);
        readCertif rd = new readCertif(this.certificate);
        List<String> listuripartrca = rd.getIssueCA();
        if(!listuripartrca.isEmpty()){
            this.rootca = listuripartrca.get(0);
        }
    }

    public PublicKey getPublicKey(){
        return this.certificate.getPublicKey();
    }

    /**
     * Méthode de vérification générale du certificat de la TSL (Date du certif / révocation / signature du certificat )
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     * @throws IOException
     * @throws CRLException
     */
    public void verify() throws CertificateException, NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, CRLException {
        try {
            this.certificate.checkValidity(new Date());
            System.out.println("Certificat TSL pas encore expiré");
        } catch (CertificateExpiredException e){
            System.out.println("Certificat TSL Expiré : " + e.getMessage());
        }

        readCertif rd = new readCertif(this.certificate);
        BigInteger serialnb = this.certificate.getSerialNumber();
        List<String> crluri = rd.getCRL();

        for(String crl : crluri){
            InputStream is = new URL(crl).openStream();
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509CRL crlcert = (X509CRL)cf.generateCRL(is);
            if(crlcert.getRevokedCertificate(serialnb)!=null){
                System.out.println("Erreur : Certif TSL est révoqué");
                break;
            }
        }
        System.out.println("Certif TSL n'est pas révoqué");
        boolean verifsignTSL = verifySignature();
        if(verifsignTSL){
            System.out.println("Signature Certif TSL OK");
        }
        else{
            System.out.println("Signature Certif TSL PAS OK");
        }
    }

    /**
     * Méthode permettant de vérifier la signature du certificat de la TSL
     * @return true si la signature est OK false sinon
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws CertificateException
     * @throws SignatureException
     * @throws IOException
     */
    public boolean verifySignature() throws NoSuchAlgorithmException, InvalidKeyException, CertificateException, SignatureException, IOException {
        CertificateFactory fact = CertificateFactory.getInstance("X.509");
        if(this.rootca.equals("")){
            return false;
        }
        InputStream is = new URL(this.rootca).openStream();
        X509Certificate certifracine= (X509Certificate) fact.generateCertificate(is);
        PublicKey pk = certifracine.getPublicKey();
        Signature sign =Signature.getInstance("SHA256withRSA");
        sign.initVerify(pk);
        sign.update(this.certificate.getTBSCertificate());
        boolean b = sign.verify(this.certificate.getSignature());
        return b;

    }

    public String getUri() {
        return uri;
    }

    /**
     * Set l'URI selon l'ID de l'AC du 2Ddoc pour pouvoir récupérer le certificat du participant
     * @param ac : Id de l'ac (i.e FR01)
     */
    public void setUri(String ac) {
        switch (ac){
            case "FR01":
                this.uri = "http://cert.pki-2ddoc.ariadnext.fr/pki-2ddoc.der";
                break;
            case "FR02":
                this.uri = "http://pki-2ddoc.sunnystamp.com/certs/pki_fr02_rfc4387_certstore_file.der";
                break;
            case "FR03":
                this.uri = "http://certificates.certigna.fr/search.php";
                break;
            case "FR04":
                this.uri = "http://pki-g2.ariadnext.fr/pki-2ddoc.der";
                break;
        }
    }


}
