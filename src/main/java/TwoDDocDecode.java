import org.apache.commons.codec.binary.Base32;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.DERSequence;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class TwoDDocDecode{
    private String entete ="";
    private String message = "";
    private HashMap<String,String> messageparse= new HashMap<String, String>();
    private HashMap<String,String> enteteparse = new HashMap<String, String>();
    private String signature = "";

    /**
     * Constructeur prenant en paramettre un code barre 2Ddoc et decode son entete / message / signature
     * @param f : Image 2Ddoc
     * @throws IOException
     */
    TwoDDocDecode(File f) throws IOException {
        String decodedText = TwoDocReader.decodeTwoDocCode(f);
        if(decodedText == null) {
            this.entete = null;
            this.message = null;
            this.signature = null;
        } else {
            int length_entete=0;
            int version = Integer.parseInt(decodedText.substring(3,4));
            switch (version){
                case 1:
                case 2:
                    length_entete = 22;
                    break;
                case 3:
                    length_entete = 24;
                    break;
                case 4:
                    length_entete = 26;
                    break;

            }
            //Entete
                this.entete = decodedText.substring(0,length_entete);
                parseEntete(this.entete);
            //Message and Signature
            boolean b = false;
            for(int i=length_entete;i<decodedText.length();i++){
                if(decodedText.charAt(i)==31){
                    this.message = decodedText.substring(length_entete,i);
                    this.signature = decodedText.substring(i+1);
                    break;
                }
            }
            System.out.println("\n------Entête------");
            parseMessage(this.message);
            for (String i : this.enteteparse.keySet()) {
                System.out.println(i + " : " +  this.enteteparse.get(i));
            }
            System.out.println("\n------Message------");
            for (String i : this.messageparse.keySet()) {
                System.out.println(i + " : " +  this.messageparse.get(i));
            }

            System.out.println("\n------Signature------");
            System.out.println("Encoder : " + this.signature);
            Base32 base32 = new Base32();
            System.out.println("Decoder : \n" +   new String(base32.decode(this.signature)));
        }
    }

    /**
     * méthode permettant de vérifier la signature contenue dans le code barre 2Ddoc
     * @param certifparticipant : Certificat du participant
     * @return true si la signature est correcte false sinon
     * @throws IOException
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public boolean verifySignature(CertificatParticipant certifparticipant) throws IOException, NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Base32 base32 = new Base32();
        byte[] signaturebyte = base32.decode(this.signature);

        PublicKey pk = certifparticipant.getPublicKey();

        Signature sign =Signature.getInstance("SHA256withECDSA","SunEC");
        sign.initVerify(pk);

        byte[] left = Arrays.copyOfRange(signaturebyte,0,signaturebyte.length/2);
        byte[] right = Arrays.copyOfRange(signaturebyte,signaturebyte.length/2,signaturebyte.length);

        ASN1Encodable[] encode = new ASN1Encodable[]{new ASN1Integer(left),new ASN1Integer(right)};
        DERSequence der = new DERSequence(encode);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        outputStream.write( this.entete.getBytes(StandardCharsets.UTF_8));
        outputStream.write( this.message.getBytes(StandardCharsets.UTF_8));
        byte[] data = outputStream.toByteArray();
        sign.update(data);
        boolean b =sign.verify(der.getEncoded());
        return b;
    }

    /**
     * Méthode permettant de renplacer l'id du type de document contenu dans l'entête par la string correspondante
     * @param id : id du type de document
     * @return le type du document en String
     */
    private String getTypeDocument(String id){
        switch (id){
            case "12":
                return "Acte d'huissier";
            case "B1":
                return  "Attestation de Versement de la Contribution à la Vie Etudiante";
            case "A8":
                return "Certificat de cession électronique";
            case "13":
                return "Document étranger";
            case "00":
                return "Justificatif de domicile";
            case "01":
                return "Facture";
            case "02":
                return "Avis de taxe d’habitation";
            case "03":
                return "Relevé d’identité bancaire";
            case "04":
                return "Avis d’impôt sur le revenu";
            case "06":
                return "Bulletin de salaire";
            case "05":
                return "Relevé d’Identité SEPAmail";
            case "07":
                return "Titre d’identité";
            case "08":
                return "MRZ";
            case "09":
                return "Document fiscal";
            case "10":
                return "Contrat de travail";
            case "11":
                return "Relevé de compte";
            case "A0":
                return "Certificat de qualité de l’air";
            case "A1":
                return "Courrier Permis à points";
            case "A2":
                return "Carte Mobilité Inclusion";
            case "A3":
                return "Macaron VTC";
            case "A4":
                return "Certificat de décès";
            case "A5":
                return "Carte T3P";
            case "A6":
                return "Carte Professionnelle Sapeur-Pompier";
            case "A7":
                return "Certificat de Qualité de l’Air (V2)";
            case "B0":
                return "Diplôme";
            default:
                return "document inconnu";
        }
    }

    /**
     * Méthode qui convertie le format de date Hex en format de date classique dd/mm/yyyy
     * @param datef2ddoc date au format Hex
     * @return date au format dd/mm/yyyy
     */
    private String parseDateEntete(String datef2ddoc){
        int nbjour = Integer.parseInt(datef2ddoc,16);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,2000);
        cal.set(Calendar.MONTH,Calendar.JANUARY);
        cal.set(Calendar.DAY_OF_MONTH,1);
        cal.add(Calendar.DAY_OF_MONTH,nbjour);
        Date d = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(d);
    }

    /**
     * Méthode permettant de parser l'entête du 2Ddoc et de sauvegarder les informations dans une HashMap(identifiant/value)
     * @param entete entete du 2Ddoc
     */
    private void parseEntete(String entete){
        this.enteteparse.put("Marqueur d’identification",entete.substring(0,2));
        this.enteteparse.put("Version",entete.substring(2,4));
        this.enteteparse.put("Id AC",entete.substring(4,8));
        this.enteteparse.put("Id certif",entete.substring(8,12));
        this.enteteparse.put("Date d’émission du document",parseDateEntete(entete.substring(12,16)));
        this.enteteparse.put("Date de creation de la signature",parseDateEntete(entete.substring(16,20)));
        this.enteteparse.put("Type de document",getTypeDocument(entete.substring(20,22)));
        switch (this.enteteparse.get("Version")){
            case "03":
                this.enteteparse.put("Identifiant du périmètre",entete.substring(22,24));
                break;
            case "04":
                this.enteteparse.put("Identifiant du périmètre",entete.substring(22,24));
                this.enteteparse.put("Pays émetteur du document",entete.substring(24,26));
                break;
        }


    }

    /**
     * Méthode permettant de parser le message du 2Ddoc et de sauvegarder les informations dans une HashMap(identifiant/value)
     * @param message : message du 2Ddoc
     */
    private void parseMessage(String message){
        int index = 0;

        while(index<message.length()){
            switch (message.charAt(index)){
                case '0':
                    index++;
                    switch (message.charAt(index)) {
                        case '1':
                            index = UpdateHashMap("Identifiant unique du document", message, 100, index, false);
                            break;
                        case '2':
                            index = UpdateHashMap("Catégorie de document", message, 100, index, false);
                            break;
                        case '3':
                            index = UpdateHashMap("Sous-catégorie de document", message, 100, index, false);
                            break;
                        case '4':
                            index = UpdateHashMap("Application de composition", message, 100, index, false);
                            break;
                        case '5':
                            index = UpdateHashMap("Version de l’application de composition", message, 100, index, false);
                            break;
                        case '6':
                            index = UpdateHashMap("Date de l’association entre le document et le code 2D-Doc.\n", message, 4, index, true);
                            break;
                        case '7':
                            index = UpdateHashMap("Heure de l’association entre le document et le code 2D-Doc.", message, 6, index, true);
                            break;
                        case '8':
                            index = UpdateHashMap("Date d’expiration du document", message, 4, index, true);
                            break;
                        case '9':
                            index = UpdateHashMap("Nombre de pages du document", message, 4, index, true);
                            break;
                        case 'A':
                            index = UpdateHashMap("Editeur du 2D-Doc", message, 9, index, true);
                            break;
                        case 'B':
                            index = UpdateHashMap("Intégrateur du 2D-Doc", message, 9, index, true);
                            break;
                        case 'C':
                            index = UpdateHashMap("URL du document", message, 100, index, false);
                            break;
                    }
                    break;
				case '1':
                    index++;
                    switch (message.charAt(index)){
                        case '0' :
                            index = UpdateHashMap("Ligne 1 de la norme adresse postale du bénéficiaire de la prestation",message,38,index,false);
                            break;
                        case '1':
                            index = UpdateHashMap("Qualité et/ou titre de la personne bénéficiaire de la prestation",message,38,index,false);
                            break;
                        case '2':
                            index = UpdateHashMap("Prénom de la personne bénéficiaire de la prestation",message,38,index,false);
                            break;
                        case '3':
                            index = UpdateHashMap("Nom de la personne bénéficiaire de la prestation",message,38,index,false);
                            break;
                        case '4':
                            index = UpdateHashMap("Ligne 1 de la norme adresse postale du destinataire de la facture",message,38,index,false);
                            break;
                        case '5':
                            index = UpdateHashMap("Qualité et/ou titre de la personne destinataire de la facture",message,38,index,false);
                            break;
                        case '6':
                            index = UpdateHashMap("Prénom de la personne destinataire de la facture",message,38,index,false);
                            break;
                        case '7':
                            index = UpdateHashMap("Nom de la personne destinataire de la facture",message,38,index,false);
                            break;
                        case '8':
                            index = UpdateHashMap("Numéro de la facture",message,9999,index,false);
                            break;
                        case '9':
                            index = UpdateHashMap("Numéro de client",message,9999,index,false);
                            break;
                        case 'A':
                            index = UpdateHashMap("Numéro du contrat",message,9999,index,false);
                            break;
                        case 'B':
                            index = UpdateHashMap("Identifiant du souscripteur du contrat",message,9999,index,false);
                            break;
                        case 'C':
                            index = UpdateHashMap("Date d’effet du contrat",message,8,index,true);
                            break;
                        case 'D':
                            index = UpdateHashMap("Montant TTC de la facture",message,16,index,false);
                            break;
                        case 'E':
                            index = UpdateHashMap("Numéro de téléphone du bénéficiaire de la prestation",message,30,index,false);
                            break;
                        case 'F':
                            index = UpdateHashMap("Numéro de téléphone du destinataire de la facture",message,30,index,false);
                            break;
                        case 'G':
                            index = UpdateHashMap("Présence d’un co-bénéficiaire de la prestation non mentionné dans le code",message,1,index,true);
                            break;
                        case 'H':
                            index = UpdateHashMap("Présence d’un co-destinataire de la facture non mentionné dans le code",message,1,index,true);
                            break;
                        case 'I':
                            index = UpdateHashMap("Ligne 1 de la norme adresse postale du co-bénéficiaire de la prestation.",message,38,index,false);
                            break;
                        case 'J':
                            index = UpdateHashMap("Qualité et/ou titre du co-bénéficiaire de la prestation",message,38,index,false);
                            break;
                        case 'K':
                            index = UpdateHashMap("Prénom du co-bénéficiaire de la prestation.",message,38,index,false);
                            break;
                        case 'L':
                            index = UpdateHashMap("Nom du co-bénéficiaire de la prestation.",message,38,index,false);
                            break;
						case 'M' :
							index = UpdateHashMap("Ligne 1de la norme adresse postale du co-destinataire de la facture.",message,38,index,false);
							break;
						case 'N' :
							index = UpdateHashMap("Qualité et/ou titre du co-destinataire de la facture.",message,38,index,false);
							break;
						case 'O' :
							index = UpdateHashMap("Prénom du co-destinataire de la facture.",message,38,index,false);
							break;
						case 'P' :
							index = UpdateHashMap("Nom du co-destinataire de la facture.",message,38,index,false);
							break;
                    }
					break;
				case '2':
					index++;
                    switch (message.charAt(index)){
                        case '0' :
							index = UpdateHashMap("Ligne 2 de la norme adresse postale du point de service des prestations",message,38,index,false);
                            break;
						case '1' :
							index = UpdateHashMap("Ligne 3 de la norme adresse postale du point de service des prestations",message,38,index,false);
                            break;
						case '2' :
							index = UpdateHashMap("Ligne 4 de la norme adresse postale du point de service des prestations",message,38,index,false);
                            break;
						case '3' :
							index = UpdateHashMap("Ligne 5 de la norme adresse postale du point de service des prestations",message,38,index,false);
                            break;
						case '4' :
							index = UpdateHashMap("Code postal ou code cedex du point de service des prestations",message,5,index,true);
                            break;
						case '5' :
							index = UpdateHashMap("Localité de destination ou libellé cedex du point de service des prestations",message,32,index,false);
                            break;
						case '6' :
							index = UpdateHashMap("Pays de service des prestations",message,2,index,true);
                            break;
						case '7' :
							index = UpdateHashMap("Ligne 2 de la norme adresse postale du destinataire de la facture",message,38,index,false);
                            break;
						case '8' :
							index = UpdateHashMap("Ligne 3 de la norme adresse postale du destinataire de la facture",message,38,index,false);
                            break;
						case '9' :
							index = UpdateHashMap("Ligne 4 de la norme adresse postale du destinataire de la facture",message,38,index,false);
                            break;
						case 'A' :
							index = UpdateHashMap("Ligne 5 de la norme adresse postale du destinataire de la facture",message,38,index,false);
                            break;
						case 'B' :
							index = UpdateHashMap("Code postal ou code cedex du destinataire de la facture",message,5,index,true);
                            break;
						case 'C' :
							index = UpdateHashMap("Localité de destination ou libellé cedex du destinataire de la facture",message,32,index,false);
                            break;
						case 'D' :
							index = UpdateHashMap("Pays du destinataire de la facture",message,2,index,true);
                            break;
					}
					break;
				case '3':
					index++;
                    switch (message.charAt(index)){
                        case '0' :
							index = UpdateHashMap("Qualité Nom et Prénom.",message,140,index,false);
                            break;
						case '1' :
							index = UpdateHashMap("Code IBAN",message,38,index,false);
                            break;
						case '2' :
							index = UpdateHashMap("Code BIC/SWIFT",message,11,index,false);
                            break;
						case '3' :
							index = UpdateHashMap("Code BBAN",message,30,index,false);
                            break;
						case '4' :
							index = UpdateHashMap("Pays de localisation du compte",message,2,index,true);
                            break;
						case '5' :
							index = UpdateHashMap("Identifiant SEPAmail (QXBAN)",message,34,index,false);
                            break;
						case '6' :
							index = UpdateHashMap("Date de début de période",message,4,index,true);
                            break;
						case '7' :
							index = UpdateHashMap("Date de fin de période",message,4,index,true);
                            break;
						case '8' :
							index = UpdateHashMap("Solde compte début de période",message,11,index,false);
                            break;
						case '9' :
							index = UpdateHashMap("Solde compte fin de période",message,11,index,false);
                            break;
					}
					break;
				case '4':
					index++;
                    switch (message.charAt(index)){
                        case '0' :
							index = UpdateHashMap("Numéro fiscal",message,13,index,true);
                            break;
						case '1' :
							index = UpdateHashMap("Revenu fiscal de référence",message,9999,index,false);
                            break;
						case '2' :
							index = UpdateHashMap("Situation du foyer",message,9999,index,false);
                            break;
						case '3' :
							index = UpdateHashMap("Nombre de parts",message,9999,index,false);
                            break;
						case '4' :
							index = UpdateHashMap("Référence d’avis d’impôt",message,13,index,true);
                            break;
					}
					break;
				case '5':
                    index++;
                    switch (message.charAt(index)){
                        case '0' :
                            index = UpdateHashMap("SIRET de l’employeur",message,14,index,true);
                            break;
                        case '1':
                            index = UpdateHashMap("Nombre d’heures travaillées",message,6,index,true);
                            break;
                        case '2':
                            index = UpdateHashMap("Cumul du nombre d’heures travaillées",message,7,index,true);
                            break;
                        case '3':
                            index = UpdateHashMap("Début de période",message,4,index,true);
                            break;
                        case '4':
                            index = UpdateHashMap("Fin de période",message,4,index,true);
                            break;
                        case '5':
                            index = UpdateHashMap("Date de début de contrat",message,8,index,true);
                            break;
                        case '6':
                            index = UpdateHashMap("Date de fin de contrat",message,4,index,true);
                            break;
                        case '7':
                            index = UpdateHashMap("Date de signature du contrat",message,8,index,true);
                            break;
                        case '8':
                            index = UpdateHashMap("Salaire net imposable",message,11,index,false);
                            break;
                        case '9':
                            index = UpdateHashMap("Cumul du salaire net imposable",message,12,index,false);
                            break;
                        case 'A':
                            index = UpdateHashMap("Salaire brut du mois",message,11,index,false);
                            break;
                        case 'B':
                            index = UpdateHashMap("Cumul du salaire brut",message,12,index,false);
                            break;
                        case 'C':
                            index = UpdateHashMap("Salaire nett",message,11,index,false);
                            break;
                        case 'D':
                            index = UpdateHashMap("Ligne 2 de la norme adresse postale de l’employeur",message,38,index,false);
                            break;
                        case 'E':
                            index = UpdateHashMap("Ligne 3 de la norme adresse postale de l’employeur",message,38,index,false);
                            break;
                        case 'F':
                            index = UpdateHashMap("Ligne 4 de la norme adresse postale de l’employeur",message,38,index,false);
                            break;
                        case 'G':
                            index = UpdateHashMap("Ligne 5 de la norme adresse postale de l’employeur",message,38,index,false);
                            break;
                        case 'H':
                            index = UpdateHashMap("Code postal ou code cedex de l’employeur",message,5,index,true);
                            break;
                        case 'I':
                            index = UpdateHashMap("Localité de destination ou libellé cedex de l’employeur",message,32,index,false);
                            break;
                        case 'J':
                            index = UpdateHashMap("Pays de l’employeur",message,2,index,true);
                            break;
                        case 'K':
                            index = UpdateHashMap("Identifiant Cotisant Prestations Sociales",message,50,index,false);
                            break;
					}
					break;
					
				case '6':
                    index++;
                    switch (message.charAt(index)){
                        case '0' :
                            index = UpdateHashMap("Liste des prénoms",message,60,index,false);
                            break;
                        case '1':
                            index = UpdateHashMap("Prénom",message,20,index,false);
                            break;
                        case '2':
                            index = UpdateHashMap("Nom patronymique",message,38,index,false);
                            break;
                        case '3':
                            index = UpdateHashMap("Nom d’usage",message,38,index,false);
                            break;
                        case '4':
                            index = UpdateHashMap("Nom d’épouse/époux",message,38,index,false);
                            break;
                        case '5':
                            index = UpdateHashMap("Type de pièce d’identité",message,2,index,true);
                            break;
                        case '6':
                            index = UpdateHashMap("Numéro de la pièce d’identité",message,20,index,false);
                            break;
                        case '7':
                            index = UpdateHashMap("Nationalité",message,2,index,true);
                            break;
                        case '8':
                            index = UpdateHashMap("Genre",message,1,index,true);
                            break;
                        case '9':
                            index = UpdateHashMap("Date de naissance",message,8,index,true);
                            break;
                        case 'A':
                            index = UpdateHashMap("Lieu de naissance",message,32,index,false);
                            break;
                        case 'B':
                            index = UpdateHashMap("Département du bureau émetteur",message,3,index,true);
                            break;
                        case 'C':
                            index = UpdateHashMap("Pays de naissance",message,2,index,true);
                            break;
                        case 'D':
                            index = UpdateHashMap("Nom et prénom du père",message,60,index,false);
                            break;
                        case 'E':
                            index = UpdateHashMap("Nom et prénom de la mère",message,60,index,false);
                            break;
                        case 'F':
                            index = UpdateHashMap("Machine Readable Zone (Zone de Lecture Automatique, ZLA)",message,90,index,false);
                            break;
                        case 'G':
                            index = UpdateHashMap("Nom",message,38,index,false);
                            break;
                        case 'H':
                            index = UpdateHashMap("Civilité",message,10,index,false);
                            break;
                        case 'I':
                            index = UpdateHashMap("Pays émetteur",message,2,index,true);
                            break;
                        case 'J':
                            index = UpdateHashMap("Type de document étranger",message,1,index,true);
                            break;
                        case 'K':
                            index = UpdateHashMap("Numéro de la demande de document étranger",message,19,index,true);
                            break;
                        case 'L':
                            index = UpdateHashMap("Date de dépôt de la demande",message,8,index,true);
                            break;
						case 'M' :
							index = UpdateHashMap("Catégorie du titre",message,40,index,false);
							break;
						case 'N' :
							index = UpdateHashMap("Date de début de validité",message,8,index,true);
							break;
						case 'O' :
							index = UpdateHashMap("Date de fin de validité",message,8,index,true);
							break;
						case 'P' :
							index = UpdateHashMap("Autorisation",message,40,index,false);
							break;
						case 'Q' :
							index = UpdateHashMap("Numéro d’étranger",message,10,index,false);
							break;
						case 'R' :
							index = UpdateHashMap("Numéro de visa",message,12,index,true);
							break;
						case 'S' :
							index = UpdateHashMap("Ligne 2 de l'adresse postale du domicile",message,38,index,false);
							break;
						case 'T' :
							index = UpdateHashMap("Ligne 3 de l'adresse postale du domicile",message,38,index,false);
							break;
						case 'U' :
							index = UpdateHashMap("Ligne 4 de l'adresse postale du domicile",message,38,index,false);
							break;
						case 'V' :
							index = UpdateHashMap("Ligne 5 de l'adresse postale du domicile",message,38,index,false);
							break;
						case 'W' :
							index = UpdateHashMap("Code postal ou code cedex de l'adresse postale du domicile",message,5,index,true);
							break;
						case 'X' :
							index = UpdateHashMap("Commune de l'adresse postale du domicile",message,32,index,false);
							break;
						case 'Y' :
							index = UpdateHashMap("Code pays de l'adresse postale du domicile",message,2,index,true);
							break;
                    }
					break;
				case '7':
                    index++;
                    switch (message.charAt(index)){
                        case '0' :
                            index = UpdateHashMap("Date et heure du décès",message,12,index,true);
                            break;
                        case '1':
                            index = UpdateHashMap("Date et heure du constat de décès",message,12,index,true);
                            break;
                        case '2':
                            index = UpdateHashMap("Nom du défunt",message,38,index,false);
                            break;
                        case '3':
                            index = UpdateHashMap("Prénoms du défunt",message,60,index,false);
                            break;
                        case '4':
                            index = UpdateHashMap("Nom de jeune filledu défunt",message,38,index,false);
                            break;
                        case '5':
                            index = UpdateHashMap("Date de naissance du défunt",message,8,index,true);
                            break;
                        case '6':
                            index = UpdateHashMap("Genre du défunt",message,1,index,true);
                            break;
                        case '7':
                            index = UpdateHashMap("Commune dedécès",message,45,index,false);
                            break;
                        case '8':
                            index = UpdateHashMap("Code postal de la commune de décès",message,5,index,true);
                            break;
                        case '9':
                            index = UpdateHashMap("Adresse du domicile du défunt",message,114,index,false);
                            break;
                        case 'A':
                            index = UpdateHashMap("Code postal du domicile du défunt",message,5,index,true);
                            break;
                        case 'B':
                            index = UpdateHashMap("Commune du domicile du défunt",message,45,index,false);
                            break;
                        case 'C':
                            index = UpdateHashMap("Obstacle médico-légal",message,1,index,true);
                            break;
                        case 'D':
                            index = UpdateHashMap("Mise en bière",message,1,index,true);
                            break;
                        case 'E':
                            index = UpdateHashMap("Obstacle aux soins de conservation",message,1,index,true);
                            break;
                        case 'F':
                            index = UpdateHashMap("Obstacle aux dons du corps",message,1,index,true);
                            break;
                        case 'G':
                            index = UpdateHashMap("Recherche de la cause du décès",message,1,index,true);
                            break;
                        case 'H':
                            index = UpdateHashMap("Délai de transport du corps",message,2,index,true);
                            break;
                        case 'I':
                            index = UpdateHashMap("Prothèse avec pile",message,1,index,true);
                            break;
                        case 'J':
                            index = UpdateHashMap("Retrait de la pile de prothèse",message,1,index,true);
                            break;
                        case 'K':
                            index = UpdateHashMap("Code NNC",message,13,index,true);
                            break;
                        case 'L':
                            index = UpdateHashMap("Code Finess de l'organisme agréé",message,9,index,true);
                            break;
						case 'M' :
							index = UpdateHashMap("Identification du médecin",message,64,index,false);
							break;
						case 'N' :
							index = UpdateHashMap("Lieu de validation du certificatde décès",message,128,index,false);
							break;
						case 'O' :
							index = UpdateHashMap("Certificat de décès supplémentaire",message,1,index,true);
							break;
					}
					break;
				case '8':
                    index++;
                    switch (message.charAt(index)){
                        case '0' :
                            index = UpdateHashMap("Nom",message,38,index,false);
                            break;
                        case '1':
                            index = UpdateHashMap("Prénoms",message,60,index,false);
                            break;
                        case '2':
                            index = UpdateHashMap("Numéro de carte",message,20,index,false);
                            break;
                        case '3':
                            index = UpdateHashMap("Organisme de tutelle",message,40,index,false);
                            break;
                        case '4':
                            index = UpdateHashMap("Profession",message,40,index,false);
                            break;
					}
					break;
				case '9':
                    index++;
                    switch (message.charAt(index)){
                        case '0' :
                            index = UpdateHashMap("Identité de l'huissier de justice",message,38,index,false);
                            break;
                        case '1':
                            index = UpdateHashMap("Identité ou raison sociale du demandeur",message,38,index,false);
                            break;
                        case '2':
                            index = UpdateHashMap("Identité ou raison sociale du destinataire",message,38,index,false);
                            break;
                        case '3':
                            index = UpdateHashMap("Identité ou raison sociale de tiers concerné",message,38,index,false);
                            break;
                        case '4':
                            index = UpdateHashMap("Intitulé de l'acte",message,38,index,false);
                            break;
                        case '5':
                            index = UpdateHashMap("Numéro de l'acte",message,38,index,false);
                            break;
                        case '6':
                            index = UpdateHashMap("Date de signature de l'acte",message,8,index,true);
                            break;
					}
					break;
                case 'A':
                    index++;
                    switch (message.charAt(index)){
                        case '0' :
                            index = UpdateHashMap("Pays émétteur de l'immatriculation du véhicule",message,2,index,true);
                            break;
                        case '1' :
                            index = UpdateHashMap("Immatriculation du véhicule",message,17,index,false);
                            break;
                        case '2' :
                            index = UpdateHashMap("Marque du véhicule",message,17,index,false);
                            break;
                        case '3' :
                            index = UpdateHashMap("Nom commercial du véhicule",message,17,index,false);
                            break;
                        case '4' :
                            index = UpdateHashMap("Numéro de série du véhicule",message,17,index,true);
                            break;
                        case '5' :
                            index = UpdateHashMap("Catégorie du véhicule",message,3,index,true);
                            break;
                        case '6':
                            index = UpdateHashMap("Carburant",message,2,index,true);
                            break;
                        case '7':
                            index = UpdateHashMap("Taux d’émission de CO2 du véhicule",message,3,index,true);
                            break;
                        case '8':
                            index = UpdateHashMap("Indication de la classe environnementale de réception CE",message,12,index,false);
                            break;
                        case '9':
                            index = UpdateHashMap("Classe d’émission polluante",message,3,index,true);
                            break;
                        case 'A':
                            index = UpdateHashMap("Date de première immatriculation du véhicule",message,8,index,true);
                            break;
                        case 'B':
                            index = UpdateHashMap("Type de lettre",message,8,index,false);
                            break;
                        case 'C':
                            index = UpdateHashMap("N° Dossier",message,19,index,false);
                            break;
                        case 'D':
                            index = UpdateHashMap("Date Infraction",message,4,index,true);
                            break;
                        case 'E':
                            index = UpdateHashMap("Heure de l’infraction",message,4,index,true);
                            break;
                        case 'F':
                            index = UpdateHashMap("Nombre de points retirés lors de l’infraction",message,1,index,true);
                            break;
                        case 'G':
                            index = UpdateHashMap("Solde de points",message,1,index,true);
                            break;
                        case 'H':
                            index = UpdateHashMap("Numéro de la carte",message,30,index,false);
                            break;
                        case 'I':
                            index = UpdateHashMap("Date d’expiration initiale",message,8,index,true);
                            break;
                        case 'J':
                            index = UpdateHashMap("Numéro EVTC",message,13,index,true);
                            break;
                        case 'K':
                            index = UpdateHashMap("Numéro de macaron",message,7,index,true);
                            break;
                        case 'L':
                            index = UpdateHashMap("Numéro de la carte",message,11,index,true);
                            break;
                        case 'M':
                            index = UpdateHashMap("Motif de sur-classement",message,5,index,false);
                            break;
                        case 'N':
                            index = UpdateHashMap("Kilométrage",message,8,index,true);
                            break;
                    }
                 break;
                case 'B' :
                    index++;
                    switch (message.charAt(index)){
                        case '0' :
                            index = UpdateHashMap("Liste des prénoms",message,60,index,false);
                            break;
                        case '1' :
                            index = UpdateHashMap("Prénom",message,20,index,false);
                            break;
                        case '2' :
                            index = UpdateHashMap("Nom patronymique",message,38,index,false);
                            break;
                        case '3' :
                            index = UpdateHashMap("Nom d'usage",message,38,index,false);
                            break;
                        case '4' :
                            index = UpdateHashMap("Nom d'épouse/époux",message,38,index,false);
                            break;
                        case '5' :
                            index = UpdateHashMap("Nationalité",message,2,index,true);
                            break;
                        case '6':
                            index = UpdateHashMap("Genre",message,1,index,true);
                            break;
                        case '7':
                            index = UpdateHashMap("Date de naissance",message,8,index,true);
                            break;
                        case '8':
                            index = UpdateHashMap("Lieu de naissance",message,32,index,false);
                            break;
                        case '9':
                            index = UpdateHashMap("Pays de naissance",message,2,index,true);
                            break;
                        case 'A':
                            index = UpdateHashMap("Mention obtenue",message,1,index,true);
                            break;
                        case 'B':
                            index = UpdateHashMap("Numéro ou code d'identification de l'étudiant",message,50,index,false);
                            break;
                        case 'C':
                            index = UpdateHashMap("Numéro du diplôme",message,20,index,false);
                            break;
                        case 'D':
                            index = UpdateHashMap("Niveau du diplôme selon la classification CEC",message,1,index,true);
                            break;
                        case 'E':
                            index = UpdateHashMap("Crédits ECTS obtenus",message,3,index,true);
                            break;
                        case 'F':
                            index = UpdateHashMap("Année universitaire",message,3,index,true);
                            break;
                        case 'G':
                            index = UpdateHashMap("Type de diplôme",message,2,index,true);
                            break;
                        case 'H':
                            index = UpdateHashMap("Domaine",message,30,index,false);
                            break;
                        case 'I':
                            index = UpdateHashMap("Mention",message,30,index,false);
                            break;
                        case 'J':
                            index = UpdateHashMap("Spécialité",message,30,index,false);
                            break;
                        case 'K':
                            index = UpdateHashMap("Numéro de l'Attestation de versement de la CVE",message,14,index,true);
                            break;
                    }
                 break;
                case 'C' :
                    index++;
                    switch (message.charAt(index)){
                        case '0' :
                            index = UpdateHashMap("Genre du vendeur",message,1,index,true);
                            break;
                        case '1' :
                            index = UpdateHashMap("Nom patronymique du vendeur",message,38,index,false);
                            break;
                        case '2' :
                            index = UpdateHashMap("Prénom du vendeur",message,20,index,false);
                            break;
                        case '3' :
                            index = UpdateHashMap("Date et heure de la cession",message,12,index,true);
                            break;
                        case '4' :
                            index = UpdateHashMap("Date de la signature du vendeur",message,8,index,true);
                            break;
                        case '5' :
                            index = UpdateHashMap("Genre de l’acheteur",message,1,index,true);
                            break;
                        case '6':
                            index = UpdateHashMap("Nom patronymique de l’acheteur",message,38,index,false);
                            break;
                        case '7':
                            index = UpdateHashMap("Prénom de l’acheteur",message,20,index,false);
                            break;
                        case '8':
                            index = UpdateHashMap("Ligne 4 de la norme adresse postale du domicile de l’acheteur",message,38,index,false);
                            break;
                        case '9':
                            index = UpdateHashMap("Code postal ou code cedex du domicile de l’acheteur",message,5,index,true);
                            break;
                        case 'A':
                            index = UpdateHashMap("Commune du domicile de l’acheteur",message,45,index,false);
                            break;
                        case 'B':
                            index = UpdateHashMap("N° d’enregistrement",message,10,index,true);
                            break;
                        case 'C':
                            index = UpdateHashMap("Date et heure d'enregistrement dans le SIV",message,12,index,true);
                            break;
                    }
                break;
            }
            index++;
        }
    }



    /**
     * Méthode permettant d'update la hashmap correspondant au message parser du 2DDOC en fonction de l'ID
     * @param key : identifiant de la donnée correspondant à un ID
     * @param message : le message du 2Ddoc
     * @param taillevalue : la taille de la chaine de caractère
     * @param index : offset du message
     * @param isfixe : true si taille fixe sinon false pour taille variable
     * @return la nouvelle valeur de l'offset pour poursuivre le parsing du messageS
     */
    private Integer UpdateHashMap(String key,String message,Integer taillevalue,Integer index,boolean isfixe){
        int length = 0;
        if(isfixe){
            index++;
            this.messageparse.put(key,message.substring(index,index+taillevalue));
            index+=taillevalue-1;
        }
        else {
            index++;
            length = Math.min(index + taillevalue, message.length());
            for(int i=index;i<length;i++){
                if(message.charAt(i)==29 || message.charAt(i)==30){
                    this.messageparse.put(key,message.substring(index,i));
                    index = i;
                    break;
                }
                else if(i==length-1){
                    this.messageparse.put(key,message.substring(index,i+1));
                    index = i;
                    break;
                }
            }
        }
        return index;

    }

    public HashMap<String, String> getEnteteparse() {
        return enteteparse;
    }

    @Override
    public String toString() {
        return String.format("Entete = "+ entete +"\n Message = "+ message + "\n Signature = " + signature);
    }
}