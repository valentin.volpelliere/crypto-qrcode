import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.*;
import java.security.cert.*;
import java.util.Date;
import java.util.List;

public class CertificatAC {
    private X509Certificate certificate= null;
    private String algo ="";
    private String rootca = "";

    /**
     * Constructeur permettant d'initialiser  le certificat de l'AC en fonction  du name contenu dans le 2Ddoc ou de l'url de l'AC
     * contenu dans le certificat du participant
     * @param nameAC : name de l'AC contenu dans le 2Ddoc (i.e FR01)
     * @param urlAC : url menant à un certificat d'une AC
     * @throws IOException
     * @throws CertificateException
     */
    public CertificatAC(String nameAC,String urlAC) throws IOException, CertificateException {
        CertificateFactory fact = CertificateFactory.getInstance("X.509");

        X509Certificate cer = null;
        if(urlAC.equals("")){
            FileInputStream is = new FileInputStream("src/main/resources/certifAC/certificat" + nameAC + ".pem");
            cer = (X509Certificate) fact.generateCertificate(is);
        }
        else {
            InputStream is = new URL(urlAC).openStream();
            cer = (X509Certificate) fact.generateCertificate(is);
        }
        this.algo = cer.getSigAlgName();
        this.certificate = cer;

        readCertif rd = new readCertif(this.certificate);
        List<String> listuripartrca = rd.getIssueCA();
        if(!listuripartrca.isEmpty()){
            this.rootca = listuripartrca.get(0);
        }

    }

    /**
     * Méthode de vérification générale du certificat de l'AC (Date du certif / révocation / signature du certificat )
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     * @throws IOException
     * @throws CRLException
     */
    public void verify() throws CertificateException, NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, CRLException {
        try {
            this.certificate.checkValidity(new Date());
            System.out.println("Certificat AC pas encore expiré");
        } catch (CertificateExpiredException e){
            System.out.println("Certificat Expiré : " + e.getMessage());
        }

        readCertif rd = new readCertif(this.certificate);
        BigInteger serialnb = this.certificate.getSerialNumber();
        List<String> crluri = rd.getCRL();

        for(String crl : crluri){
            InputStream is = new URL(crl).openStream();
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509CRL crlcert = (X509CRL)cf.generateCRL(is);
            if(crlcert.getRevokedCertificate(serialnb)!=null){
                System.out.println("Erreur : Certif AC est révoqué");
                break;
            }
        }
        System.out.println("Certif AC n'est pas révoqué");
        boolean verifsignAC= verifySignature();
        if(verifsignAC){
            System.out.println("Signature Certif AC OK");
        }
        else{
            System.out.println("Signature Certif AC PAS OK");
        }
    }

    /**
     * Méthode permettant de vérifier la signature du certificat de l'AC
     * @return true si la signature est OK false sinon
     * @throws NoSuchAlgorithmException
     * @throws CertificateException
     * @throws SignatureException
     * @throws InvalidKeyException
     * @throws IOException
     */
    public boolean verifySignature() throws NoSuchAlgorithmException, CertificateException, SignatureException, InvalidKeyException, IOException {
        PublicKey pk = null;
        if(!this.rootca.equals("")){
            CertificateFactory fact = CertificateFactory.getInstance("X.509");
            InputStream is = new URL(this.rootca).openStream();
            X509Certificate certifracine= (X509Certificate) fact.generateCertificate(is);
            pk = certifracine.getPublicKey();
            this.algo = certifracine.getSigAlgName();
        }
        else {
            pk = getPublicKey();
        }
        Signature sign =Signature.getInstance(this.algo);
        sign.initVerify(pk);
        sign.update(this.certificate.getTBSCertificate());
        boolean b = sign.verify(this.certificate.getSignature());
        return b;
    }

    public PublicKey getPublicKey(){
        return this.certificate.getPublicKey();
    }

    public String getAlgo() {
        return algo;
    }

    public void setAlgo(String algo) {
        this.algo = algo;
    }
}
