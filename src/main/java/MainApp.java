import java.io.*;
import java.security.*;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;


public class MainApp {
    public static void main(String[] args) {
      try {
            File file = new File("src/main/resources/img/FR03.png"); //Chemin de l'image 2DDoc (à modifier manuellement)
            TwoDDocDecode DDoc= new TwoDDocDecode(file);

            //TSL
            TSL tsl = new TSL(DDoc.getEnteteparse().get("Id AC"));
            String uri =tsl.getUri() + "?name=" + DDoc.getEnteteparse().get("Id certif");
            System.out.println("\n-----Verification----");
            tsl.verify();
            //Certif Participant
            CertificatParticipant certifpart = new CertificatParticipant(uri);
          //Certif AC
            CertificatAC ac = new CertificatAC(DDoc.getEnteteparse().get("Id AC"),certifpart.getUriCA());

            ac.verify();
            certifpart.verify(ac);

            //Verification de la signature 2Ddoc
            boolean verifsign=DDoc.verifySignature(certifpart);
            if(verifsign){
                System.out.println("Signature 2Ddoc OK");
            }
            else {
                System.out.println("Signature 2Ddoc PAS OK");
            }

      } catch (IOException e) {
            System.out.println("Could not decode 2Ddoc Code, IOException :: " + e.getMessage());
        } catch (CertificateException | NoSuchAlgorithmException | InvalidKeyException | SignatureException | NoSuchProviderException | CRLException e) {
          e.printStackTrace();
      }
    }


}
