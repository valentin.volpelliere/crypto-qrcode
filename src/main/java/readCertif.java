import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.x509.*;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class readCertif {

    private X509Certificate certificate;

    /**
     * Constructeur prenant en paramettre le certificat à lire
      * @param certificate : le certificat à lire
     */
    public readCertif(X509Certificate certificate) {
        this.certificate = certificate;
    }

    /**
     * Méthode permettant de récupérer les url menant à la ca ou root ca d'un certificat
     * @return la liste d'url permettant de récupérer le certificat d'une ca ou rootca
     * @throws IOException
     */
    public List<String> getIssueCA() throws IOException {
        byte[] crlDPExtensionValue = this.certificate.getExtensionValue("1.3.6.1.5.5.7.1.1");
        List<String> caUrls = new ArrayList<String>();
        if(crlDPExtensionValue != null) {
            ASN1InputStream asn1In = new ASN1InputStream(crlDPExtensionValue);
            AuthorityInformationAccess distPoint;
            DEROctetString crlDEROctetString = (DEROctetString) asn1In.readObject();
            //Get Input stream in octets
            ASN1InputStream asn1InOctets = new ASN1InputStream(crlDEROctetString.getOctets());
            ASN1Primitive crlDERObject = asn1InOctets.readObject();
            distPoint = AuthorityInformationAccess.getInstance(crlDERObject);


            for (AccessDescription dp : distPoint.getAccessDescriptions()) {

                if (dp.getAccessMethod().getId().equals("1.3.6.1.5.5.7.48.2")) {
                    GeneralName genName = dp.getAccessLocation();

                    String url = DERIA5String.getInstance(genName.getName()).getString().trim();
                    caUrls.add(url);
                }
            }
        }

        return caUrls;
    }

    /**
     * Méthode permettant de récupérer les urls menant à la crl du certificat
     * @return la liste d'url permettant de récupérer la crl du certificat
     * @throws IOException
     */
    public List<String> getCRL() throws IOException {
        byte[] crlDPExtensionValue = this.certificate.getExtensionValue("2.5.29.31");
        List<String> crlUrls = new ArrayList<String>();
        if (crlDPExtensionValue != null){

            ASN1InputStream asn1In = new ASN1InputStream(crlDPExtensionValue);

            CRLDistPoint distPoint;
            DEROctetString crlDEROctetString = (DEROctetString) asn1In.readObject();
            //Get Input stream in octets
            ASN1InputStream asn1InOctets = new ASN1InputStream(crlDEROctetString.getOctets());
            ASN1Primitive crlDERObject = asn1InOctets.readObject();
            distPoint = CRLDistPoint.getInstance(crlDERObject);

            for (DistributionPoint dp : distPoint.getDistributionPoints()) {

                DistributionPointName dpn = dp.getDistributionPoint();
                if (dpn != null && dpn.getType() == DistributionPointName.FULL_NAME) {

                    GeneralName[] genNames = GeneralNames.getInstance(dpn.getName()).getNames();

                    for (GeneralName genName : genNames) {
                        if (genName.getTagNo() == GeneralName.uniformResourceIdentifier) {

                            String url = DERIA5String.getInstance(genName.getName()).getString().trim();
                            crlUrls.add(url);
                        }
                    }
                }
            }

        }
        return crlUrls;

    }

}
